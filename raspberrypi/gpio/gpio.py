import RPi.GPIO as GPIO
import time

LedPin12 = 12     # pin12
LedPin13 = 13     # pin13

def setup():
  GPIO.setmode(GPIO.BOARD)       # Numbers GPIOs by physical location
  GPIO.setup(LedPin12, GPIO.OUT)
  GPIO.output(LedPin12, GPIO.LOW)
  GPIO.setup(LedPin13, GPIO.OUT)
  GPIO.output(LedPin13, GPIO.LOW)

def destroy():  
  GPIO.output(LedPin12, GPIO.LOW)
  GPIO.output(LedPin13, GPIO.LOW)
  GPIO.cleanup()               # Release resource

if __name__ == '__main__':     # Program start from here
  setup()
  try:
    print("Abryt med CTRL + C")
    while True:
      input12 = input("orange 1 or 0")
      if input12 == 1:
        GPIO.output(LedPin12, GPIO.HIGH)
        print("setting orange to HIGH")
      input13 = input("yellow 1 or 0:")
      if input13 == 1:
        GPIO.output(LedPin13, GPIO.HIGH)
        print("setting yellow to HIGH")
      print("sleeping 5 seconds")
      time.sleep(5)
      GPIO.output(LedPin12, GPIO.LOW)
      GPIO.output(LedPin13, GPIO.LOW)
     

  except KeyboardInterrupt:  # When 'Ctrl+C' is pressed, the child program dest$
    destroy()
